// app代表应用对象
module.exports = app => {
  // 先得到 mongoose的模块，通过它得到骨架模型
  let mongoose = app.mongoose; 
  // 先定义Schema ,通过它定义集合里文档中的属性和类型
  let Schema = mongoose.Schema;
  // 用户集合的模型骨架，他不连接数据库也不能操作数据库
  let CategorySchema = new Schema({
    name: String,
  });
  // 返回一个数据模型，用户模型是可以对数据库进行增删改查的
  // 如果不定义第二个参数表示，获取mode
  return mongoose.model('Category', CategorySchema);

}