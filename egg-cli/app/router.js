'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  // 注册
  router.post('/api/users/signup', controller.users.signup);
  // 登录
  router.post('/api/users/signin', controller.users.signin);
  // 退出
  router.get('/api/users/signout', controller.users.signout);

  router.resources('categories', '/api/categories', controller.categories);

  router.resources('articles', '/api/articles', controller.articles);
  // restful接口规范 get => index  post => create  put => update  delete => destory
  // router.get('/api/categories', controller.categories.index);
  // router.post('/api/categories', controller.categories.create);
  // router.put('/api/categories/:id', controller.categories.update);
  // router.delete('/api/categories/:id', controller.categories.destory);

  // 阅读量 与评论 接口
  router.get('/api/articles/pv/:id', controller.articles.addPv);
  router.post('/api/articles/comment/:id', controller.articles.addComment);


};
