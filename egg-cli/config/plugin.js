'use strict';

exports.mongoose = {
  enable: true,
  package: 'egg-mongoose'
}

// 后端跨域的插件
exports.cors = {
  enable: true,
  package: 'egg-cors'
}