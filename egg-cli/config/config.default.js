/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1567300176709_7983';

  // add your middleware config here
  config.middleware = [];

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };
  // 配置 mongoose 是node里面操作mongodb的一个模块，可以以对象的形式来操作数据库
  config.mongoose = {
    url: 'mongodb://127.0.0.1/egg-cli'

  }
  // csrf安全配置， 白名单配置
  config.security = {
    csrf: false,
    domainWhiteList: ['http://localhost:3000']
  }

  config.cors = {
    credentials: true
  }

  return {
    ...config,
    ...userConfig,
  };
};
