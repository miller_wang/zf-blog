exports.keys = 'zfpx'; // 用来加密cookie的

exports.view = {
  defaultViewEngine: 'ejs', // 默认渲染引擎
  // 设置针对什么类型的文件，用什么模板引擎进行渲染
  mapping: {
    // 如果是.ejs文件，用ejs进行渲染
    '.ejs': 'ejs'
  }
}