
const {Controller} = require('egg');

class NewsController extends Controller {

  async index(){
    let {ctx} = this
    // 后端渲染
    let news = [{
      title: '标题',
      url: 'http://www.baidu.com'
    },{
      title: '标题',
      url: 'http://www.baidu.com'
    }]
    // 异步需要等待响应完成
    await ctx.render('news.ejs', {news})
  }
}

module.exports = NewsController;