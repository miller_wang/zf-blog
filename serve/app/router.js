// egg 
/**
 * router 路由容器
 * controller  控制器容器
 * controller = {
 *  home
 * }
 * controller会找  controller目录中对应的类
 * controller.home = new HomeController()
 */
module.exports = (app) => {
  const {router, controller} = app;
  // 当客户端以get方式请求/时，
  router.get('/', controller.home.index);

  router.get('/news', controller.news.index);

}