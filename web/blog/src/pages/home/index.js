import React, { PureComponent, Component } from 'react'
import {Form, Input, Icon, Button, message, Row, Col} from 'antd';
import {signup, signin} from '../../service/user'
export default class componentName extends PureComponent {
  render() {
    return (
      <div className="home-page">
        <div className="login-form">
          <h1>欢迎光临珠峰博客</h1>
          <WrappedUserForm 
            onSubmit={this.handleSubmit.bind(this)}>
          </WrappedUserForm>
        </div>
      </div>
    )
  }

  handleSubmit(isSignUp, data){
    console.log(this)
    let actionFn = isSignUp? signup: signin;
    actionFn(data).then(res => {
      if (res.code === 0) {
        if(!isSignUp){
          sessionStorage.setItem('UserName', res.data.username);
        }
        // 跳转页面
        this.props.history.push('/admin');
      }else{
        message.error(res.error);
      }
      console.log(res)
    })
  }
}

class UserFrom extends Component{
  render(){
    const {getFieldDecorator} = this.props.form;
    // <Icon type="mobile" />
    return (
      <Form onSubmit={(e) => {
        e.preventDefault();
        this.props.onSubmit( this.state.isSignUp, this.props.form.getFieldsValue() );
      }}>
          <Form.Item>
            {
              getFieldDecorator('username', {
                rules: [{validator: this.checkUsername} ,{required: true, message: '请输入用户名'}]
              })(<Input prefix={<Icon type="mobile" style={{color: 'rgba(0,0,0,.25)'}} />} placeholder="请输入用户名" />)
            }
          </Form.Item>
          <Form.Item>
            {
              getFieldDecorator('password', {
                rules: [{required: true, message: '请输入密码'}]
              })(<Input type="password" prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}} />} placeholder="请输入密码" />)
            }
          </Form.Item>
          {
            this.state.isSignUp && 
            <Form.Item>
              {
                getFieldDecorator('email', {
                  rules: [{required: true, message: '请输入邮箱'}]
                })(<Input type="email" prefix={<Icon type="mail" style={{color: 'rgba(0,0,0,.25)'}} />} placeholder="请输入邮箱" />)
              }
            </Form.Item>
          }

          <Form.Item className="login-bottom">
            <Row>
              <Col span={8}>
                <Button htmlType="submit" className="login-form-btn">
                  {this.state.isSignUp? '注册': '登录'}
                </Button>
              </Col>

              <Col span={12}>
                <Button onClick={() => this.setState({isSignUp: !this.state.isSignUp})}>
                  {this.state.isSignUp? '已有账号，直接登录': '没有账号，注册账号'}
                </Button>
              </Col>
            </Row>
          </Form.Item>
        </Form>
    )
  }

  constructor(props){
    super(props);
    this.state = {
      isSignUp: true, // 默认是注册表单

    }
  }

  checkUsername = (rule, value, callback) => {
    if (!value) {
      callback('用户名不能为空')
    }else if (!/^1\d{10}$/.test(value)){
      callback('用户名必须是一个手机号')
    }else{
      callback(); // 不传参数表示成功
    }
  }
}

const WrappedUserForm = Form.create()(UserFrom)

