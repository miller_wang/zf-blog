import React, { PureComponent, Component } from 'react'
import {Form, Input, Icon, Button, message, Row, Col, Switch} from 'antd';
import {Route} from 'react-router-dom';

import Header from '../../components/Header/index';
import NavLeft from '../../components/NavLeft/index';
import Welcome from '../welcome/index';
import Category from '../category/index';
import Article from '../article/index';


export default class Admin extends Component {
  render(){
    return (
      <div className="admin-page">
        <Row>
          <Col span={24}><Header></Header></Col>
        </Row>

        <Row>
          <Col span={5}>
            <NavLeft></NavLeft>
          </Col>
          <Col span={18}>
            <Route exact path="/admin" component={Welcome}></Route>
            <Route path="/admin/category" component={Category}></Route>
            <Route path="/admin/article" component={Article}></Route>
          </Col>
        </Row>
      </div>
    );
  }
  

}

// alipays://platformapi/startapp?appId=20000067&url=https%3A%2F%2Fopenapi.alipay.com%2Fgateway.do%3Fapp_id%3D2019072465960461%26method%3Dalipay.user.certify.open.certify%26charset%3Dutf-8%26format%3DJSON%26timestamp%3D2019-09-07%2B20%253A21%253A47%26version%3D1.0%26sign_type%3DRSA2%26biz_content%3D%257B%2522certify_id%2522%253A%25222145348069d3765a818fcb0bb195c4cb%2522%257D%26sign%3DnBMtdbNBCJZfKxlFpg1MKQbFAggN8Bi9ccP0vmSOG2v4Ct%252B6%252B0gIJT4ovpQ3fY0s%252B7aT8f3h4rm1Jwqlj1csa2PlccA%252Fhfsmk2hYd%252FeaSH0wY%252FhHD0FQBDBA%252BDIcDiyZhqwjz4wcSSMvvAaU5s%252BvQCsBUuPE0gEjF2Rs8QkH404H37j3%252FI%252FbqhzuB%252B2W1AqC%252BXHfNkF5X5fcdtneBLl%252BorTB6cQTKxd1xsNK5SVFF35tVsOn0U4qhhNTrR75XB2wTS8%252FplGL7QNMw49mNo4UKGYRpB0GaKRlOiwJ%252BJy3xYNVYM0Go%252BEfjwwEFCLYhwhckkiU6Uz3wvDT9EQyeRxzsw%253D%253D

// alipays://platformapi/startapp?appId=20000067&url=https%3A%2F%2Fopenapi.alipay.com%2Fgateway.do%3Fapp_id%3D2019072465960461%26method%3Dalipay.user.certify.open.certify%26charset%3Dutf-8%26format%3DJSON%26timestamp%3D2019-09-07%2012%3A52%3A30%26    version%3D1.0%26sign_type%3DRSA2%26biz_content%3D%7B%22    certify_id%22%3A%2268e5aae92d5d685d28c0972f197f0914%22%7D%26sign%3DKZO6DvEI2zZtRAh70tEsfy73SCaMbBZ5Jg%2Bjtb%2FTugjUe0s9bStqwgUUK1QRXvsaht%2BgYWB1NiD9p4fA39ySScXqdEbF00LzCO2dee3%2FycLrtdQrjYBoPFa8iUSxDdAii0PQs6ii5Kt64T08HyJ5TA%2BXIXfI7XDScxUmAfhAOxRyy1juKy6SjsmhMVWD52gK7rmjbTaIXaWuS2JarLihU3zBdfMJbfXkErTtetX0jNq8V16Iu2AcZgeyRieah33E6RHaBEYxjcgg3ICW1h71WKD6S2fWPtf%2B70YA9Qw71kCEcGPNxXV%2BeKuy6dUBR5EviHqI6V8rF%2FULOywiWHcFCg%3D%3D%26certify_id%3D68e5aae92d5d685d28c0972f197f0914%26