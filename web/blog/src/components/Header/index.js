import React, {Component} from 'react';
import { Row, Col, Icon, Button, message } from 'antd';
import {withRouter} from 'react-router-dom';
import {signout} from '../../service/user'
class Header extends Component{
  render(){
    return (
      <Row className="admin-header">
        <Col span={6} offset={1}>
          <h2>珠峰博客</h2>
        </Col>
        <Col span={10} offset={7}>
          <Icon type="smile"></Icon>
          用户 {this.state.username}
          <Button onClick={this.logout.bind(this)}>
            <Icon type="logout"></Icon>
            退出
          </Button>
          
          
        </Col>
      </Row>
    );
  }
  constructor(){
    super();
    this.state = {
      username: ''
    }
  }

  logout = () => {
    signout().then(res => {
      if (res.code == 0) {
        sessionStorage.removeItem('UserName');
        this.props.history.push('/');
      }else{
        message.error(res.error);
      }
    })
  }

  componentWillMount(){
    let username = sessionStorage.getItem('UserName');
    this.setState({username});
  }
}

export default withRouter(Header);