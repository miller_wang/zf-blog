import React, {Component} from 'react';
import {Menu, Icon} from 'antd';
import {withRouter, Link} from 'react-router-dom';
 class NavLeft extends Component{
  render() {
    return (
       <Menu
        defaultSelectedKeys={[window.location.pathname]}
        // onClick={this.handleClick}
       >
         <Menu.Item key="/admin"> 
          <Link to="/admin">
            <Icon type="lock" />首页
          </Link>
        </Menu.Item>
        <Menu.Item key="/admin/category"> 
          <Link to="/admin/category">
            <Icon type="lock" />分类管理
          </Link>
        </Menu.Item>
        <Menu.Item key="/admin/article">
          <Link to="/admin/article">
            <Icon type="mail" />文章管理
          </Link>
        </Menu.Item>
       </Menu>
    );
  }

  componentDidMount() {
    console.log('location',window.location.hash.slice(1))
  }
  // 第一种跳转方式  第二种使用Link
  handleClick = (item) => {
    console.log('点击菜单', item)
    this.props.history.push(item.key);
  }
}

export default withRouter(NavLeft)
