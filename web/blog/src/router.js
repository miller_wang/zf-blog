import React, { Component } from "react";
// import {HashRouter as Router, Route, Switch} from 'react-router-dom';
import {Router, Route, Switch} from 'react-router-dom';
import { createBrowserHistory } from "history";
import Home from './pages/home/index';
import Admin from './pages/admin/index';

// HashRouter 内置了history hashHistory

// Router 需要用到createBrowserHistory
const history = createBrowserHistory();
// 每当路由变化时，都会执行这个监听函数
history.listen(loc => {
  console.log('路由监听', loc);
  if (loc.pathname == '/admin' && !sessionStorage.getItem('UserName')) {
    history.push('/')
  }
});

export default class Routers extends Component{
  render() {
    return (
       <Router history={history}>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/admin" component={Admin} />
        </Switch>
       </Router>
    );
  }
}