import {get, post, put, del} from './index';
const ENTITY = '/api/users'
// 注册
export const signup = (data) => post(`${ENTITY}/signup`, data);
// 登录
export const signin = (data) => post(`${ENTITY}/signin`, data);

// 登录
export const signout = () => get(`${ENTITY}/signout`);