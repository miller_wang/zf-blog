import {get, post, put, del} from './index';
// 将对象 解析成 url参数 
import qs from "qs";
const ENTITY = '/api/articles'

// 获取分类列表
const getArticleList = (param) => get(`${ENTITY}?${qs.stringify(param)}`)

const addArticle = (param) => post(`${ENTITY}`, param)

const updateArticle = (param) => put(`${ENTITY}/${param.id}`, param)

const delArticle = (param) => del(`${ENTITY}/${param.id}`, param)

// 查看文章
const checkArticle = (param) => get(`${ENTITY}/pv/${param.id}`)
// 评论文章
const commentArticle = (param) => post(`${ENTITY}/comment/${param.id}`, param)


export default {
  getArticleList, 
  addArticle, 
  updateArticle, 
  delArticle, 
  checkArticle, 
  commentArticle
};