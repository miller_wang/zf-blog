import axios from 'axios';
const baseUrl = 'http://127.0.0.1:7001';

const config = {
  baseUrl,
  timeout: 8000,
  withCredentials: true // 跨域请求的时候携带cokie
}

function get(url){
  return axios({
    ...config,
    method: 'get',
    url
  }).then(res => res.data);
}

function post(url, data){
  return axios({
    ...config,
    method: 'post',
    url,
    data
  }).then(res => res.data);
}

function put(url, data){
  return axios({
    ...config,
    method: 'put',
    url,
    data
  }).then(res => res.data);
}

function del(url, data){
  return axios({
    ...config,
    method: 'delete',
    url,
    data
  }).then(res => res.data);
}

export { get, post, put, del }