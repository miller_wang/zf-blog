import {get, post, put, del} from './index';
// 将对象 解析成 url参数 
import qs from "qs";
const ENTITY = '/api/categories'

// 获取分类列表
export const getCategoryList = (param) => get(`${ENTITY}?${qs.stringify(param)}`)

export const addCategory = (param) => post(`${ENTITY}`, param)

export const updateCategory = (param) => put(`${ENTITY}/${param.id}`, param)

export const delCategory = (param) => del(`${ENTITY}/${param.id}`, param)